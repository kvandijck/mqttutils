#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <math.h>
#include <sys/time.h>

#include <unistd.h>
#include <getopt.h>
#include <poll.h>
#include <syslog.h>
#include <mosquitto.h>

#include "common.h"

/* terminal codes, copied from can-utils */

#define CLR_SCREEN  "\33[2J"
#define CSR_HOME  "\33[H"
#define ATTRESET "\33[0m"

#define NAME "mqttqv"

/* program options */
static const char help_msg[] =
	NAME ": MQTT spy\n"
	"usage:	" NAME " [OPTIONS ...] [TOPIC ...]\n"
	"\n"
	"Options\n"
	" -V, --version		Show version\n"
	" -v, --verbose		Verbose output\n"
	"\n"
	" -m, --maxperiod=TIME	Consider TIME as maximum period (default 5s).\n"
	"			Slower rates are considered multiple one-time ID's\n"
	" -x, --remove=TIME	Remove ID's after TIME (default 20s).\n"
	"\n"
	" -h, --host=HOST[:PORT]Specify alternate MQTT host+port\n"
	" -q, --qos=LEVEL	Set outgoing MQTT qos (0..2, default 1)\n"
	" -t			dummy option\n"
	;
#ifdef _GNU_SOURCE
static struct option long_opts[] = {
	{ "help", no_argument, NULL, '?', },
	{ "version", no_argument, NULL, 'V', },
	{ "verbose", no_argument, NULL, 'v', },

	{ "remove", required_argument, NULL, 'x', },
	{ "maxperiod", required_argument, NULL, 'm', },

	{ "host", required_argument, NULL, 'h', },
	{ "qos", required_argument, NULL, 'q', },
	{ },
};
#else
#define getopt_long(argc, argv, optstring, longopts, longindex) \
	getopt((argc), (argv), (optstring))
#endif
static const char optstring[] = "V?vx:m:h:q:t";
static int verbose;
static double deadtime = 20.0;
static double maxperiod = 5.0;

static const char *mqtt_host = "localhost";
static int mqtt_port = 1883;
static int mqtt_keepalive = 10;
static int mqtt_qos = 1;

/* state */
static struct mosquitto *mosq;

/* jiffies, in msec */
static double jiffies;

static void update_jiffies(void)
{
	struct timeval tv;

	gettimeofday(&tv, NULL);
	jiffies = tv.tv_sec + tv.tv_usec/1e6;
}

/* cache definition */
struct cache {
	char *topic;
	char *payload;

	int flags;
		#define F_DIRTY		0x01
	double lastrx;
	double period;
};
static struct cache *cache;
static int ncache, scache;

static int cmpcache(const void *va, const void *vb)
{
	const struct cache *a = va, *b = vb;

	return strcmp(a->topic, b->topic);
}

static void mqtt_msg_recvd(struct mosquitto *mosq, void *dat, const struct mosquitto_message *msg)
{
	struct cache *curr, w = { .topic = msg->topic, };

	curr = bsearch(&w, cache, ncache, sizeof(*cache), cmpcache);
	if (!curr && (ncache >= scache)) {
		/* grow cache */
		scache += 16;
		cache = realloc(cache, sizeof(*cache)*scache);
		if (!cache)
			mylog(LOG_ERR, "realloc %u cache: %s", scache, ESTR(errno));
		memset(cache+ncache, 0, (scache - ncache)*sizeof(*cache));
	}
	if (!curr) {
		/* add in cache */
		curr = cache+ncache++;
		curr->flags |= F_DIRTY;
		curr->topic = strdup(msg->topic);
		curr->payload = strdup(msg->payload ?: "");
		curr->period = NAN;
		curr->lastrx = jiffies;
		qsort(cache, ncache, sizeof(*cache), cmpcache);
	} else {
		if (strcmp(curr->payload, msg->payload ?: "")) {
			curr->flags |= F_DIRTY;
			/* update cache */
			free(curr->payload);
			curr->payload = strdup(msg->payload ?: "");
		}
		curr->period = jiffies - curr->lastrx;
		if (curr->period > maxperiod)
			curr->period = NAN;
		curr->lastrx = jiffies;
	}
}

int main(int argc, char *argv[])
{
	int opt, ret, j;
	char *str;
	struct cache *curr;
	double last_update, lastseen;
	char mqtt_name[128];

	/* argument parsing */
	while ((opt = getopt_long(argc, argv, optstring, long_opts, NULL)) != -1)
	switch (opt) {
	case 'V':
		fprintf(stderr, "%s %s, "
				"Compiled on %s %s\n",
				NAME, VERSION, __DATE__, __TIME__);
		exit(0);
		break;
	default:
		fprintf(stderr, "%s: unknown option '%u'\n\n", NAME, opt);
	case '?':
		fputs(help_msg, stderr);
		return opt != '?';
	case 'v':
		++verbose;
		break;
	case 'x':
		deadtime = strtod(optarg, NULL);
		break;
	case 'm':
		maxperiod = strtod(optarg, NULL);
		break;
	case 'h':
		mqtt_host = optarg;
		str = strrchr(optarg, ':');
		if (str > mqtt_host && *(str-1) != ']') {
			/* TCP port provided */
			*str = 0;
			mqtt_port = strtoul(str+1, NULL, 10);
		}
		break;
	case 'q':
		mqtt_qos = strtoul(optarg, NULL, 0);
		break;
	case 't':
		break;
	}

	/* MQTT start */
	mosquitto_lib_init();
	sprintf(mqtt_name, "%s-%i", NAME, getpid());
	mosq = mosquitto_new(mqtt_name, true, 0);
	if (!mosq)
		mylog(LOG_ERR, "mosquitto_new failed: %s", ESTR(errno));
	/* mosquitto_will_set(mosq, "TOPIC", 0, NULL, mqtt_qos, 1); */

	mosquitto_message_callback_set(mosq, mqtt_msg_recvd);

	ret = mosquitto_connect(mosq, mqtt_host, mqtt_port, mqtt_keepalive);
	if (ret)
		mylog(LOG_ERR, "mosquitto_connect %s:%i: %s", mqtt_host, mqtt_port, mosquitto_strerror(ret));

	if (optind >= argc) {
		ret = mosquitto_subscribe(mosq, NULL, "#", mqtt_qos);
		if (ret)
			mylog(LOG_ERR, "mosquitto_subscribe '#': %s", mosquitto_strerror(ret));
	} else for (; optind < argc; ++optind) {
		ret = mosquitto_subscribe(mosq, NULL, argv[optind], mqtt_qos);
		if (ret)
			mylog(LOG_ERR, "mosquitto_subscribe %s: %s", argv[optind], mosquitto_strerror(ret));
	}

	/* pre-init cache */
	scache = ncache = 0;
	cache = NULL;

	last_update = 0;
	while (1) {
		ret = mosquitto_loop(mosq, 1, 1);
		if (ret)
			mylog(LOG_ERR, "mosquitto_subscribe %s: %s", argv[optind], mosquitto_strerror(ret));

		update_jiffies();

		if ((jiffies - last_update) < 0.5)
			continue;
		/* remove dead cache */
		for (j = 0; j < ncache; ++j) {
			curr = cache+j;
			lastseen = jiffies - curr->lastrx;

			if (lastseen > deadtime) {
				/* delete this entry */
				free(curr->topic);
				free(curr->payload);
				memcpy(cache+j, cache+j+1, (ncache-j-1)*sizeof(*cache));
				--ncache;
				--j;
				continue;
			}

			if (!isnan(curr->period) && (lastseen > 2*curr->period))
				/* reset period */
				curr->period = NAN;
		}

		last_update = jiffies;
		/* update screen */
		puts(CLR_SCREEN ATTRESET CSR_HOME);
		for (j = 0; j < ncache; ++j) {
			curr = cache+j;
			printf("%s%-*c: %s", curr->topic, (int)(40 - strlen(curr->topic)), ' ', curr->payload);
			/*
			printf("\tlast=-%.3lfs", jiffies - cache[j].lastrx);
			*/
			if (verbose >= 2 && !isnan(cache[j].period))
				printf("\t\tperiod=%.3lfs", cache[j].period);
			printf("\n");
			cache[j].flags &= F_DIRTY;
		}
	}
	return 0;
}

