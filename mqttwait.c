#include <ctype.h>
#include <errno.h>
#include <signal.h>
#include <stdarg.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <unistd.h>
#include <getopt.h>
#include <syslog.h>
#include <mosquitto.h>

#include "common.h"

#define NAME "mqttwait"
#ifndef VERSION
#define VERSION "<undefined version>"
#endif

/* program options */
static const char help_msg[] =
	NAME ": a simple MQTT topic waiting tool\n"
	"usage:	" NAME " [OPTIONS ...] TOPIC [REGEX ...]\n"
	"\n"
	"Options\n"
	" -V, --version		Show version\n"
	" -v, --verbose		Be more verbose\n"
	" -h, --host=HOST[:PORT][/PREFIX/][?qos=X&keepalive=Y] Specify alternate MQTT uri\n"
	" -m, --maxtime=TIME	Limit waiting time\n"
	" -R			Discard retained messages, i.e. only wait for new messages\n"
	;

#ifdef _GNU_SOURCE
static struct option long_opts[] = {
	{ "help", no_argument, NULL, '?', },
	{ "version", no_argument, NULL, 'V', },
	{ "verbose", no_argument, NULL, 'v', },

	{ "host", required_argument, NULL, 'h', },
	{ "maxtime", required_argument, NULL, 'm', },

	{ },
};
#else
#define getopt_long(argc, argv, optstring, longopts, longindex) \
	getopt((argc), (argv), (optstring))
#endif
static const char optstring[] = "Vv?h:m:R";

/* logs */
static int loglevel = LOG_WARNING;

/* state */
static const char *mqtt_host = "localhost";
static int mqtt_port = 1883;
static int mqtt_keepalive = 10;
static int mqtt_qos = 1;

static int mqtt_maxtime = 0;
static char **needles;
static int allowretained = 1;

/* signal handler */
static volatile int sigterm;
static volatile int sigalrm;
static volatile int result = 1;

static void onsignal(int sig)
{
	switch (sig) {
	case SIGTERM:
	case SIGINT:
		sigterm = 1;
		break;
	case SIGALRM:
		sigalrm = 1;
		break;
	}
}

static void my_mqtt_msg(struct mosquitto *mosq, void *dat, const struct mosquitto_message *msg)
{
	int j;
	char *payload = (char *)msg->payload;

	if (sigterm)
		/* ignore */
		return;

	if (msg->retain && !allowretained)
		return;

	if (!*needles && msg->payloadlen > 0)
		goto matched;

	for (j = 0; needles[j]; ++j) {
		if (!strcasecmp(needles[j], payload ?: ""))
			goto matched;
	}
	return;

matched:
	printf("%.*s\n", msg->payloadlen, payload);
	/* mark program exit */
	sigterm = 1;
	result = 0;
}

int main(int argc, char *argv[])
{
	int opt;
	int ret;
	char *topic;
	char *str;

	/* argument parsing */
	while ((opt = getopt_long(argc, argv, optstring, long_opts, NULL)) >= 0)
	switch (opt) {
	case 'V':
		fprintf(stderr, "%s %s (%s %s)\n",
				NAME, VERSION, __DATE__, __TIME__);
		exit(0);
	case 'v':
		++loglevel;
		break;
	case 'h':
		mqtt_host = optarg;
		str = strrchr(optarg, ':');
		if (str > mqtt_host && *(str-1) != ']') {
			/* TCP port provided */
			*str = 0;
			mqtt_port = strtoul(str+1, NULL, 10);
		}
		break;
	case 'm':
		mqtt_maxtime = strtoul(optarg, NULL, 0);
		break;
	case 'R':
		allowretained = 0;
		break;

	default:
		fprintf(stderr, "unknown option '%c'", opt);
	case '?':
		fputs(help_msg, stderr);
		exit(1);
		break;
	}

	if (optind >= argc)
		mylog(LOG_ERR, "no topic given");
	topic = argv[optind++];
	needles = argv+optind;

	/* start program */
	myopenlog(NAME, 0, LOG_LOCAL2);
	myloglevel(loglevel);

	signal(SIGINT, onsignal);
	signal(SIGTERM, onsignal);
	signal(SIGALRM, onsignal);

	/* MQTT start */
	char mqtt_name[32];
	struct mosquitto *mosq;
	mosquitto_lib_init();
	sprintf(mqtt_name, "%s-%i", NAME, getpid());
	mosq = mosquitto_new(mqtt_name, true, 0);
	if (!mosq)
		mylog(LOG_ERR, "mosquitto_new failed: %s", ESTR(errno));

	mosquitto_message_callback_set(mosq, my_mqtt_msg);

	ret = mosquitto_connect(mosq, mqtt_host, mqtt_port, mqtt_keepalive);
	if (ret)
		mylog(LOG_ERR, "mosquitto_connect %s:%i: %s", mqtt_host, mqtt_port, mosquitto_strerror(ret));

	ret = mosquitto_subscribe(mosq, NULL, topic, mqtt_qos);
	if (ret)
		mylog(LOG_ERR, "mosquitto_subscribe '%s': %s", topic, mosquitto_strerror(ret));

	if (mqtt_maxtime)
		alarm(mqtt_maxtime);

	for (; !sigterm && !sigalrm;) {
		ret = mosquitto_loop(mosq, 1000, 1);
		if (ret)
			mylog(LOG_ERR, "mosquitto_loop: %s", mosquitto_strerror(ret));
	}
	if (sigalrm)
		mylog(LOG_ERR, "no match received within %u", mqtt_maxtime);
	mosquitto_disconnect(mosq);
	/* clean exit */
	return result;
}
