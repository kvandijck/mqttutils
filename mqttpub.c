#include <ctype.h>
#include <errno.h>
#include <signal.h>
#include <stdarg.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <unistd.h>
#include <getopt.h>
#include <syslog.h>
#include <mosquitto.h>

#include "common.h"

#define NAME "mqttpub"
#ifndef VERSION
#define VERSION "<undefined version>"
#endif

/* program options */
static const char help_msg[] =
	NAME ": an quick MQTT publishing tool\n"
	"usage:	" NAME " [OPTIONS ...] [TOPIC=PAYLOAD ...]\n"
	"\n"
	"Options\n"
	" -V, --version		Show version\n"
	" -v, --verbose		Be more verbose\n"
	" -h, --mqtt=HOST[:PORT]Specify alternate MQTT host+port\n"
	" -q, --qos=QoS		Set QoS to use (default 1)\n"
	;

#ifdef _GNU_SOURCE
static struct option long_opts[] = {
	{ "help", no_argument, NULL, '?', },
	{ "version", no_argument, NULL, 'V', },
	{ "verbose", no_argument, NULL, 'v', },

	{ "host", required_argument, NULL, 'h', },
	{ "qos", required_argument, NULL, 'q', },

	{ },
};
#else
#define getopt_long(argc, argv, optstring, longopts, longindex) \
	getopt((argc), (argv), (optstring))
#endif
static const char optstring[] = "Vv?h:q:";

/* logging */
static int loglevel = LOG_WARNING;

/* signal handler */
static volatile int sigterm;

/* MQTT parameters */
static const char *mqtt_host = "localhost";
static int mqtt_port = 1883;
static int mqtt_keepalive = 10;
static int mqtt_qos = 1;

/* state */
static struct mosquitto *mosq;
static int ready;

/* signalling */
static void onsigterm(int signr)
{
	sigterm = 1;
}

static void my_mqtt_msg(struct mosquitto *mosq, void *dat, const struct mosquitto_message *msg)
{
	if (is_self_sync(msg))
		ready = 1;
}

int main(int argc, char *argv[])
{
	int opt, ret;
	char *str, *payload, assign;
	char mqtt_name[32];
	char *line;
	size_t linesize;

	/* argument parsing */
	while ((opt = getopt_long(argc, argv, optstring, long_opts, NULL)) >= 0)
	switch (opt) {
	case 'V':
		fprintf(stderr, "%s %s\nCompiled on %s %s\n",
				NAME, VERSION, __DATE__, __TIME__);
		exit(0);
	case 'v':
		++loglevel;
		break;
	case 'h':
		mqtt_host = optarg;
		str = strrchr(optarg, ':');
		if (str > mqtt_host && *(str-1) != ']') {
			/* TCP port provided */
			*str = 0;
			mqtt_port = strtoul(str+1, NULL, 10);
		}
		break;
	case 'q':
		mqtt_qos = strtoul(optarg, NULL, 0);
		break;

	default:
		fprintf(stderr, "unknown option '%c'", opt);
	case '?':
		fputs(help_msg, stderr);
		exit(1);
		break;
	}

	myopenlog(NAME, 0, LOG_LOCAL2);
	myloglevel(loglevel);
	signal(SIGINT, onsigterm);
	signal(SIGTERM, onsigterm);

	/* MQTT start */
	mosquitto_lib_init();
	sprintf(mqtt_name, "%s-%i", NAME, getpid());
	mosq = mosquitto_new(mqtt_name, true, 0);
	if (!mosq)
		mylog(LOG_ERR, "mosquitto_new failed: %s", ESTR(errno));

	mosquitto_message_callback_set(mosq, my_mqtt_msg);

	ret = mosquitto_connect(mosq, mqtt_host, mqtt_port, mqtt_keepalive);
	if (ret)
		mylog(LOG_ERR, "mosquitto_connect %s:%i: %s", mqtt_host, mqtt_port, mosquitto_strerror(ret));

	/* parse input file */
	line = NULL;
	linesize = 0;
	int use_args = optind < argc;
	for (;;) {
		if (use_args) {
			if (optind >= argc)
				break;
			line = argv[optind++];
		} else {
			ret = getline(&line, &linesize, stdin);
			if (ret < 0) {
				if (feof(stdin))
					break;
				mylog(LOG_ERR, "readline <stdin>: %s", ESTR(errno));
			}
			if (line[ret-1] == '\n')
				/* cut newline */
				line[ret-1] = 0;
			if (!*line || *line == '#')
				/* ignore such line */
				continue;
		}
		if (!strcmp(line, "...")) {
			ready = 0;
			send_self_sync(mosq, mqtt_qos);

			/* fluah+wait */
			while (!sigterm && !ready) {
				ret = mosquitto_loop(mosq, 1000, 1);
				if (ret)
					mylog(LOG_ERR, "mosquitto_loop: %s", mosquitto_strerror(ret));
			}
			continue;
		}
		payload = strpbrk(line, "=>~");
		if (!payload)
			mylog(LOG_ERR, "'%s' bad format", line);

		assign = *payload;
		*payload++ = 0;
		/* publish topic */
		ret = mosquitto_publish(mosq, NULL, line, strlen(payload), payload, mqtt_qos, assign == '=');
		if (ret)
			mylog(LOG_ERR, "mosquitto_publish %s: %s", line, mosquitto_strerror(ret));
	}
	send_self_sync(mosq, mqtt_qos);

	/* fluah+wait */
	while (!sigterm && !ready) {
		ret = mosquitto_loop(mosq, 1000, 1);
		if (ret)
			mylog(LOG_ERR, "mosquitto_loop: %s", mosquitto_strerror(ret));
	}
	if (line && !use_args)
		free(line);
	return 0;
}
