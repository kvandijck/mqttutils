#include <errno.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <unistd.h>
#include <syslog.h>
#include <mosquitto.h>

#include "common.h"

/* error logging */
static int maxloglevel = LOG_WARNING;
static const char *label;

void myopenlog(const char *name, int options, int facility)
{
	label = name;
	/* we append \n seperately.
	 * Make stderr line-buffered, so it results in 1 single write
	 */
	setlinebuf(stderr);
}

void myloglevel(int level)
{
	maxloglevel = level;
}

void mylog(int loglevel, const char *fmt, ...)
{
	va_list va;

	if (loglevel > maxloglevel)
		goto done;
	va_start(va, fmt);
	if (label)
		fprintf(stderr, "%s: ", label);
	vfprintf(stderr, fmt, va);
	fputc('\n', stderr);
	fflush(stderr);
	va_end(va);
done:
	if (loglevel <= LOG_ERR)
		exit(1);
}

/* common float to string, with stripped trailing 0 */
const char *mydtostr(double d)
{
	static char buf[64];
	char *str;
	int ptpresent = 0;

	sprintf(buf, "%lg", d);
	for (str = buf; *str; ++str) {
		if (*str == '.')
			ptpresent = 1;
		else if (*str == 'e')
			/* nothing to do anymore */
			break;
		else if (ptpresent && *str == '0') {
			int len = strspn(str, "0");
			if (!str[len]) {
				/* entire string (after .) is '0' */
				*str = 0;
				if (str > buf && *(str-1) == '.')
					/* remote '.' too */
					*(str-1) = 0;
				break;
			}
		}
	}
	return buf;
}

/* self-sync util */
static char myuuid[128];
static const char selfsynctopic[] = "tmp/selfsync";
void send_self_sync(struct mosquitto *mosq, int qos)
{
	int ret;

	sprintf(myuuid, "%i-%li-%i", getpid(), time(NULL), rand());

	ret = mosquitto_subscribe(mosq, NULL, selfsynctopic, qos);
	if (ret)
		mylog(LOG_ERR, "mosquitto_subscribe %s: %s", selfsynctopic, mosquitto_strerror(ret));
	ret = mosquitto_publish(mosq, NULL, selfsynctopic, strlen(myuuid), myuuid, qos, 0);
	if (ret < 0)
		mylog(LOG_ERR, "mosquitto_publish %s: %s", selfsynctopic, mosquitto_strerror(ret));
}

int is_self_sync(const struct mosquitto_message *msg)
{
	return !strcmp(msg->topic, selfsynctopic) &&
		!strcmp(myuuid, msg->payload ?: "");
}
