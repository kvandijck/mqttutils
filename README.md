# mqttutils

This projects provides several small utilities built around MQTT.

## mqttretained

A fast and simple tool to retrieve a retained message from MQTT.
It uses ```tmp/selfsync``` topic to detect the end of retained messages.
It simply prints the first retained topic found and terminates.

## mqttpub

mqttpub allows to publish multiple topics at once.
This works much faster than calling ```mosquitto_pub``` for each topic.
Both *retained* and non-*retained* payloads are supported through a special syntax.
See ```mqttpub -?``` for more info

## mqttqv

aka **mqtt quick view**.

mqttqv shows recent topics+payload in a top-like view.
